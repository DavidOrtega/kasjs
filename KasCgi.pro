TEMPLATE = app

TARGET   = KasCgi

QT      += core gui webkit network

win32 {
    wLIBS    += -lwsock32
}

SOURCES += src/main.cpp

HEADERS += src/Cgi.h
SOURCES += src/Cgi.cpp

HEADERS += src/Engine.h
SOURCES += src/Engine.cpp

HEADERS += src/FileSystem.h
SOURCES += src/FileSystem.cpp

HEADERS += src/IOProcess.h
SOURCES += src/IOProcess.cpp

HEADERS += src/WebPage.h
SOURCES += src/WebPage.cpp

HEADERS += src/WebkitBrowser.h
SOURCES += src/WebkitBrowser.cpp
