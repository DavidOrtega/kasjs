var browser = new KBrowser();
	//browser.setProxy({host:'127.0.0.1', port: 8888});
	browser
	.setUserAgent('Kas Browser 1.0')
	.request( {url: 'file:///C:/MyPrograms/AppServ/www/cgi-bin/five/examples/KBrowser/test.html', jsEnabled: true} )
	.wait( {time: 10, jsAssert: ''} );
	
	dump( browser.getHTML()  );
	browser.evaluateJS("changeText();");
	dump( browser.getHTML() );
	
	//data from the browser can be passed to KAS
	dump( browser.evaluateJS("getArray();") );
	
	//DOM has changed but the original response is still available
	
	dump( browser.RESPONSE() );
	//browser.savePDF('test.pdf');
	//browser.saveBMP('test.bmp');