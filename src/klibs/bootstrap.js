//load libs
core.require('klibs/fulljslint.js',		true);
core.require('klibs/dump.js',			true);
core.require('klibs/mustache.js',		true);
core.require('klibs/MooTools.Core.js',	true);

core.require('klibs/KCGI.js',			true);
core.require('klibs/KBrowser.js',		true);
core.require('klibs/KIOProcess.js',		true);
core.require('klibs/KFileSystem.js',	true);

var doJSLINT = function (code, options, fileName)
{
	try
	{
		/*jslint white: true, onevar: true, undef: true, newcap: true, nomen: true, regexp: true, plusplus: true, bitwise: true, maxerr: 50, indent: 4 */
		if(!JSLINT(code, options))
		{
			var jsLint			= new Object();
				jsLint.fileName	= fileName;
				jsLint.errors	= JSLINT.errors;
			
			dump(jsLint);
		}

	}catch(error)
	{
		echo(error);
	}
}

var createNativeInstance = function(lib)
{
	var obj;
	
	eval('obj = ' + core.createIntance(lib) + ';');
	
	return obj;
}

function bin2string(array)
{
	var result = "";
	
	if(array)
	{
		for (var i = 0; i < array.length; i++)
		{
			result += String.fromCharCode(array[i]);
		}	
	}
	
	return result;
}

function string2bin(str)
{
	var result = [];
	
	for (var i = 0; i < str.length; i++)
	{
		result.push( str.charCodeAt(i) );
	}
	
	return result;
}

function toObject(arr) 
{
	var rv = {};
  
	for (var i = 0; i < arr.length; ++i)
	{
		rv[i] = arr[i];
	}

	rv['length'] = i;
	
	return rv;
}

var clone = function(obj)
{
	return JSON.decode( JSON.encode(obj) );
}

//set cgi enviroment
var kcgi = new KCGI();

//global functions
var CGI			= kcgi.ENVIROMENT;
var GET			= kcgi.GET;
var POST		= kcgi.POST;
var COOKIES		= kcgi.COOKIES;

var HEADERS		= core.HEADERS;
var echo		= core.echo;
var log			= core.log;
var require		= core.require;
var wait		= core.wait;

core.require( CGI['PATH_INFO'].slice(1), true);

/*

//test only
core.setPageEncoding('utf-8');
core.setHeader('content-type', 'text/html;charset=utf-8');

/** TEST ENCODING
core.setPageEncoding('utf-8');
core.setHeader('content-type', 'text/html;charset=utf-8');
core.echo('���');

var fs		= createNativeInstance('fileSystem');
var handler	= fs.fileOpen('text.txt', 'rw');
fs.fileWriteLine(handler, '���');
echo(fs.toString(fs.fileRead(handler)));

var fs	= new KFileSystem();
var fh	= fs.fileOpen('test.txt', 'rw')
	
fs.fileWriteLine(fh, 'this is a test');
fs.fileWriteLine(fh, 'this is a test2');
	
dump( fs.fileRead(fh) );

if( fs.fileExists('test.txt') )
{
	fs.fileCopy( 'test.txt', 'test2.txt');
	fs.fileRename( 'test2.txt', 'test3.txt');
	
	dump( fs.fileInfo('test3.txt') );
}
	

var browser = new KBrowser();
	//dump( browser.setProxy({host:'127.0.0.1', port: 8888}).setUserAgent('Kas Browser 1.0').request( {url: 'http://localhost/test-filepost.php'} ).wait( {time: 10, jsAssert: ''} ).getText() );
	//dump( browser.RESPONSE() );

var ioProcess = new KIOProcess();
	//ioProcess.run('java -jar tika/tika-app-0.9.jar --html').stdIn('hello tika!!!').stdInClose().wait(3);
	//dump(ioProcess.stdError());
	//echo(ioProcess.stdOut());
*/
	
