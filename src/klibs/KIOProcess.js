function KIOProcess()
{ 
	this.run = function(commmand)
	{
		nativeInstance.run(commmand);
		
		return this;
	}
	
	this.kill = function()
	{
		nativeInstance.kill();
		
		return this;
	}
	
	this.wait = function(timeout)
	{
		nativeInstance.wait(timeout);
		
		return this;
	}
	
	this.stdIn = function(data)
	{
		nativeInstance.stdIn(data);
		
		return this;
	}
	
	this.stdInClose = function()
	{
		nativeInstance.stdInClose();
		
		return this;
	}
	
	this.statusCode = function()
	{
		return nativeInstance.statusCode();
	}
	
	this.isRunning = function()
	{
		return nativeInstance.isRunning();
	}
	
	this.stdOut = function()
	{
		return bin2string( nativeInstance.stdOut() ); 
	}
	
	this.stdError = function()
	{
		return bin2string( nativeInstance.stdError() );
	}
	
	var nativeInstance	= createNativeInstance('IOProcess');
	var self			= this;
} 