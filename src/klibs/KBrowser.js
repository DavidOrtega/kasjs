function KBrowser()
{ 
	this.request = function(input)
	{
		nativeInstance.request(input);
		
		return this;
	}
	
	this.wait = function(input)
	{
		input.time		= input.time		|| 60;
		input.jsAssert	= input.jsAssert	|| '';
		
		nativeInstance.wait(input.time, input.jsAssert);
		
		return this;
	}
	
	this.saveBMP = function(fileName)
	{
		nativeInstance.saveBMP(fileName);
		
		return this;
	}
	
	this.savePDF = function(fileName)
	{
		nativeInstance.savePDF(fileName);
		
		return this;
	}
	
	this.setUserAgent = function(userAgent)
	{
		nativeInstance.setUserAgent(userAgent);
		
		return this;
	}
	
	this.setProxy = function(input)
	{
		nativeInstance.setProxy(input);
		
		return this;
	}
	
	this.RESPONSE = function()
	{
		return nativeInstance.RESPONSE;
	}
	
	this.CONTENT = function()
	{
		return bin2string(nativeInstance.CONTENT);
	}
	
	this.evaluateJS = function(javascript)
	{
		return nativeInstance.evaluateJS(javascript);
	}
	
	this.getUserAgent = function()
	{
		return nativeInstance.getUserAgent();
	}
	
	this.getProxy = function()
	{
		return nativeInstance.getProxy();
	}
	
	this.getText = function()
	{
		return nativeInstance.getText();
	}
	
	this.getHTML = function()
	{
		return nativeInstance.getHTML();
	}
	
	var nativeInstance	= createNativeInstance('webkitBrowser');
	var self			= this;
} 