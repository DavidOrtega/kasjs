function KFileSystem()
{ 
	this.fileOpen = function(fileName, mode)
	{
		return nativeInstance.fileOpen(fileName, mode || 'rw');
	}
	
	this.fileExists = function(fileName)
	{
		return nativeInstance.fileExists(fileName);
	}
	
	this.fileOpen = function(fileName, mode)
	{
		return nativeInstance.fileOpen(fileName, mode || 'rw');
	}
	
	this.fileRead = function(fileRef)
	{
		return bin2string( nativeInstance.fileRead(fileRef) );
	}
	
	this.fileReadLine = function(fileRef)
	{
		return bin2string( nativeInstance.fileRead(fileRef) );
	}
	
	this.fileCopy = function(source, target)
	{
		nativeInstance.fileCopy(source, target);
		
		if( self.fileExists(target) )
		{
			return true;
		}
		
		return false;
	}
	
	this.fileRename = function(source, target)
	{
		nativeInstance.fileRename(source, target);
		
		if( self.fileExists(target) )
		{
			return true;
		}
		
		return false;
	}
	
	this.fileRemove = function(fileName)
	{
		nativeInstance.fileRemove(fileName);
		
		if( !self.fileExists(fileName) )
		{
			return true;
		}
		
		return false;
	}
	
	this.fileInfo = function(fileName)
	{
		return nativeInstance.fileInfo(fileName);
	}
	
	this.fileWrite = function(fileRef, data)
	{
		nativeInstance.fileWrite(fileRef, data);
		
		return this;
	}
	
	this.fileWriteLine = function(fileRef, data)
	{
		self.fileWrite(fileRef, data + '\n');
		
		return this;
	}
	
	this.fileWriteBinary = function(fileRef, binary)
	{
		nativeInstance.fileWriteBinary(fileRef, binary);
		
		return this;
	}
	
	this.fileClose = function(fileRef)
	{
		nativeInstance.fileClose(fileRef);
		
		return this;
	}
	
	var nativeInstance	= createNativeInstance('fileSystem');
	var self			= this;
	var path			= CGI['PATH_INFO'].split('/').slice(0, -1).join('/') + '/';
} 