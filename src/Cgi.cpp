#include "Cgi.h"

Cgi::Cgi()
{
	this->setEnviroment();
	this->setGetVariables();
	this->setPostVariables();
}

Cgi::~Cgi(){}

QString	Cgi::getEnviromentValue(QString name)
{
	this->ENVIROMENT[name] = QUrl::fromPercentEncoding(getenv(name.toStdString().c_str()));

	return this->ENVIROMENT[name].toString();
}

void Cgi::output(QString output, int code, QString codeMessage, QString contentType)
{
	QString response  = "Status: " + QString::number(code) + " " + codeMessage + "\n";
			response += "Content-type: " + contentType + "\n\n";
			response += output.toUtf8();

			//DOES NOT WORK DELAYS THE OUTPUT!!!
			//response += "Content-length: " + QString(output.length()) + "\n\n";

	printf(response.toUtf8());
}

/****PRIVATE SECTION****/
void Cgi::setEnviroment()
{
	this->ENVIROMENT["AUTH_TYPE"]			= getEnviromentValue("AUTH_TYPE");

	this->ENVIROMENT["CONTENT_TYPE"]		= getEnviromentValue("CONTENT_TYPE");
	this->ENVIROMENT["PATH_INFO"]			= getEnviromentValue("PATH_INFO");
	this->ENVIROMENT["PATH_TRANSLATED"]		= getEnviromentValue("PATH_TRANSLATED");
	this->ENVIROMENT["REMOTE_HOST"]			= getEnviromentValue("REMOTE_HOST");
	this->ENVIROMENT["REMOTE_IDENT"]		= getEnviromentValue("REMOTE_IDENT");
	this->ENVIROMENT["REMOTE_USER"]			= getEnviromentValue("REMOTE_USER");

	this->ENVIROMENT["GATEWAY_INTERFACE"]	= getEnviromentValue("GATEWAY_INTERFACE");
	this->ENVIROMENT["SCRIPT_NAME"]			= getEnviromentValue("SCRIPT_NAME");
	this->ENVIROMENT["HTTP_ACCEPT"]			= getEnviromentValue("HTTP_ACCEPT");
	this->ENVIROMENT["HTTP_USER_AGENT"]		= getEnviromentValue("HTTP_USER_AGENT");
	this->ENVIROMENT["REQUEST_METHOD"]		= getEnviromentValue("REQUEST_METHOD");
	this->ENVIROMENT["QUERY_STRING"]		= getEnviromentValue("QUERY_STRING");
	this->ENVIROMENT["CONTENT_LENGTH"]		= getEnviromentValue("CONTENT_LENGTH");
	this->ENVIROMENT["REMOTE_ADDR"]			= getEnviromentValue("REMOTE_ADDR");
	this->ENVIROMENT["SERVER_NAME"]			= getEnviromentValue("SERVER_NAME");
	this->ENVIROMENT["SERVER_PORT"] 		= getEnviromentValue("SERVER_PORT");
	this->ENVIROMENT["SERVER_PROTOCOL"] 	= getEnviromentValue("SERVER_PROTOCOL");
	this->ENVIROMENT["SERVER_SOFTWARE"] 	= getEnviromentValue("SERVER_SOFTWARE");
}

void Cgi::setGetVariables()
{
	this->getQueryParams(this->ENVIROMENT["QUERY_STRING"].toString(), &this->GET);
}

void Cgi::setPostVariables()
{
	QTextStream stream(stdin, QIODevice::ReadOnly);
	QString 	postBlock = stream.read(this->ENVIROMENT["CONTENT_LENGTH"].toInt());

	this->getQueryParams(postBlock, &this->POST);

	this->ENVIROMENT["REQUEST_BODY"] = postBlock;
}

void Cgi::getQueryParams(QString queryString, QMap<QString, QVariant>* map)
{
	if(queryString.length())
	{
		QStringList pair = queryString.split(QRegExp("[&]"));

		for(int i=0; i<pair.length(); i++)
		{
			QStringList unit = pair[i].split(QRegExp("[=]"));

			if(unit.length() > 1)
			{
				map->insert(unit[0], unit[1]);

			}else
			{
				map->insert(unit[0], QVariant(""));
			}
		}
	}
}
