#include "WebkitBrowser.h"
#include <QtNetwork>

WebkitBrowser::WebkitBrowser(QObject *parent)
{
	this->_proxy["host"]		= "";
	this->_proxy["port"]		= "";
	this->_proxy["user"]		= "";
	this->_proxy["password"]	= "";

	this->_cookieJar = new QNetworkCookieJar();
	this->_manager.setCookieJar(this->_cookieJar);

	this->setUserAgent("Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9.1");

	connect(&this->_timer, SIGNAL(timeout()), this, SLOT(slotUpdateTimeElapsed()));
	connect(&this->_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotManagerFinished(QNetworkReply*)));
	connect(&this->_webPage, SIGNAL(loadFinished(bool)), this, SLOT(slotLoadFinished(bool)));
	connect(&this->_webPage, SIGNAL(loadStarted()), this, SLOT(slotLoadStarted()));
}

WebkitBrowser::~WebkitBrowser()
{
	this->_cookieJar->deleteLater();
}

QVariant WebkitBrowser::getRequestOption(QString key, QVariant defaultValue)
{
	if(this->_requestOptions.contains(key))
	{
		return this->_requestOptions[key];
	}

	return defaultValue;
}

/********************************SLOTS*****************************/
QVariant WebkitBrowser::RESPONSE()
{
	return QVariant(this->_response);
}

QVariant WebkitBrowser::CONTENT()
{
	return QVariant(this->_content);
}

void WebkitBrowser::setUserAgent(QString userAgent)
{
	this->_webPage.userAgent = userAgent;
}

QString WebkitBrowser::getUserAgent()
{
	return this->_webPage.userAgent;
}

void WebkitBrowser::setProxy(QVariant proxyM)
{
	this->_proxy = proxyM.toMap();

	QNetworkProxy	proxy;
					proxy.setHostName( this->_proxy["host"].toString() );
					proxy.setPort( this->_proxy["port"].toInt() );
					proxy.setUser( this->_proxy["user"].toString() );
					proxy.setPassword( this->_proxy["password"].toString() );

	if(this->_proxy["type"].toString() == "http")
	{
		proxy.setType(QNetworkProxy::HttpProxy);

	}else if(this->_proxy["type"].toString() == "ftp")
	{
		proxy.setType(QNetworkProxy::FtpCachingProxy);

	}else if(this->_proxy["type"].toString() == "socks5")
	{
		proxy.setType(QNetworkProxy::Socks5Proxy);

	}else
	{
		proxy.setType(QNetworkProxy::NoProxy);
	}

	this->_manager.setProxy(proxy);
	this->_webPage.networkAccessManager()->setProxy(proxy);
}

QVariant WebkitBrowser::getProxy()
{
	return this->_proxy;
}

void WebkitBrowser::request(QVariant options)
{
	this->_webPageLoaded	= false;
	this->_requestOptions	= options.toMap();

	QMap<QString, QVariant> emptyMap;
	QString	url						= this->getRequestOption("url", "").toString();
	QString op						= this->getRequestOption("method", "GET").toString().toLower();
	QByteArray data					= this->getRequestOption("data", "").toString().toAscii();
	QMap<QString, QVariant> headers	= this->getRequestOption("headers", emptyMap).toMap();

	QNetworkRequest networkRequest;
					networkRequest.setUrl(QUrl(url));
					networkRequest.setRawHeader("User-Agent", this->getUserAgent().toAscii());

	if( headers.size() )
	{
		QMapIterator<QString, QVariant> i(headers);
		while (i.hasNext())
		{
			i.next();
			networkRequest.setRawHeader(i.key().toAscii(), i.value().toString().toAscii());
		}
	}

	if(op == "get" && data.length() && url.contains("?"))
	{
		url = url + "&" + data;

	}else
	{
		url = url + "?" + data;
	}

	if(op == "post")
	{
		this->_manager.post(networkRequest, data);

	}else if(op == "put")
	{
		this->_manager.put(networkRequest, data);

	}else if(op == "head")
	{
		this->_manager.head(networkRequest);

	}else if(op == "delete")
	{
		//method = QNetworkAccessManager::DeleteOperation; available in 4.7

	}else
	{
		this->_manager.get(networkRequest);
	}
}

void WebkitBrowser::wait(int seconds, QString assertJavascript)
{
	this->_timeElapsed = 0;
	this->_timer.start(1000);

	bool assert	= false;

	while(!assert && (this->_timeElapsed) < seconds)
    {
		qApp->processEvents(QEventLoop::WaitForMoreEvents | QEventLoop::ExcludeUserInputEvents);

    	assert = this->_webPageLoaded;

		if(assertJavascript.length())
		{
			assert = this->evaluateJS(assertJavascript).toBool();
		}
    }

    this->_timer.stop();
}

QVariant WebkitBrowser::evaluateJS(QString javascript)
{
	return this->_webPage.mainFrame()->evaluateJavaScript(javascript);
}

QString WebkitBrowser::getHTML()
{
	return this->_webPage.mainFrame()->toHtml();
}

QString WebkitBrowser::getText()
{
	return this->_webPage.mainFrame()->toPlainText();
}

void WebkitBrowser::saveBMP(QString fileName)
{
	this->_webPage.setViewportSize(this->_webPage.mainFrame()->contentsSize());

	QImage   image(this->_webPage.viewportSize(), QImage::Format_ARGB32);
	QPainter painter(&image);

	this->_webPage.mainFrame()->render(&painter);

	painter.end();
	image.save(fileName);
}

void WebkitBrowser::savePDF(QString fileName)
{
	QPrinter 	p(QPrinter::HighResolution);
				p.setOutputFormat(QPrinter::PdfFormat);
				p.setOutputFileName(fileName);

	this->_webPage.mainFrame()->print(&p);
}

void WebkitBrowser::slotUpdateTimeElapsed()
{
	this->_timeElapsed++;
}

void WebkitBrowser::slotManagerFinished(QNetworkReply* reply)
{
	this->_response["httpStatusCode"]	= reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString();
	QString	contentType					= "text/html";

	QList<QByteArray> list = reply->rawHeaderList();
	for (int i=0; i<list.size(); ++i)
	{
		this->_response[list.at(i)] = QString(reply->rawHeader(list.at(i)));
	}

	QUrl redirectUrl(reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl());
	QUrl baseUrl( reply->url() );
	baseUrl = baseUrl.resolved(redirectUrl);

	//if redirect
	if(!redirectUrl.isEmpty() && baseUrl.toString() != reply->url().toString())
	{
		this->_requestOptions["url"] = baseUrl.toString();
		this->request(QVariant(this->_requestOptions));

	}else
	{
		this->_content = reply->readAll();

		this->_webPage.settings()->setAttribute(QWebSettings::JavascriptEnabled, this->getRequestOption("jsEnabled", "true").toBool());
		this->_webPage.settings()->setAttribute(QWebSettings::AutoLoadImages, this->getRequestOption("loadImages", "false").toBool());
		this->_webPage.networkAccessManager()->setCookieJar(this->_cookieJar);

		if(contentType.contains("text/html", Qt::CaseInsensitive))
		{
			this->_webPage.mainFrame()->setContent(	this->_content, "text/html", reply->url() );

		}else
		{
			this->_webPageLoaded = true;
		}
	}

	reply->deleteLater();
}

void WebkitBrowser::slotLoadFinished(bool ok)
{
	this->_webPageLoaded = true;
}

void WebkitBrowser::slotLoadStarted()
{
	this->_webPageLoaded = false;
}
