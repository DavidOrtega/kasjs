#ifndef IOPROCESS_H
#define IOPROCESS_H

#include <QtCore>
#include <QTimer>
#include <QProcess>
#include <qwaitcondition.h>

/*
this->_code = 0;	//SUCCESS
this->_code = 1;	//FAILED
this->_code = 2;	//CRASH
this->_code = 3;	//FILE NOT FOUND
*/

class IOProcess : public QObject
{
    Q_OBJECT

	public:
		 IOProcess(QObject *parent = 0);
		~IOProcess();

	private:
		int         _timeElapsed;
		int         _timeout;
		bool		_isRunning;
		int			_code;
		QTimer		_timer;
		QProcess	_process;
		QByteArray  _stdOut;
		QByteArray  _stdError;

	public slots:
		void		run(QString commmand);
		void		kill();
		void		wait(int timeout);
		bool		isRunning();
		void		stdIn(QByteArray data);
		void 		stdInClose();
		QByteArray	stdOut();
		QByteArray	stdError();
		int			statusCode();

	private slots:
		void slotUpdateTimeElapsed();
		void slotStarted();
		void slotStateChanged(QProcess::ProcessState newState);
		void slotReadyReadStandardOutput();
		void slotReadyReadStandardError();
		void slotProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
		void slotProcessError(QProcess::ProcessError error);
};

#endif
