#include "WebPage.h"

WebPage::WebPage(QObject *parent) : QWebPage(parent)
{
	this->userAgent = QString("");

	this->settings()->setAttribute(QWebSettings::JavaEnabled, false);
	this->settings()->setAttribute(QWebSettings::JavascriptCanOpenWindows, false);
	this->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, false);

	/*
	this->settings()->setAttribute(QWebSettings::LocalStorageDatabaseEnabled, false);
	this->settings()->setAttribute(QWebSettings::OfflineWebApplicationCacheEnabled, false);
	this->settings()->setAttribute(QWebSettings::PrivateBrowsingEnabled, true);
	this->settings()->setMaximumPagesInCache(0);
	this->settings()->setObjectCacheCapacities(0, 0, 0);
	this->history()->setMaximumItemCount(0);
	*/
}

WebPage::~WebPage(){}

QString WebPage::userAgentForUrl(const QUrl &url ) const
{
	return this->userAgent;
}

void WebPage::javaScriptConsoleMessage(const QString& message, int lineNumber, const QString& sourceID)
{
    qDebug() << message + " on line: " + QString::number(lineNumber) + " Source: " + sourceID;
}

void WebPage::javaScriptAlert(QWebFrame *originatingFrame, const QString& msg)
{
	qDebug() << "Alert: " + msg;
}

bool WebPage::javaScriptConfirm(QWebFrame *originatingFrame, const QString& msg)
{
	qDebug() << "Confirm: " + msg;

	return true;
}

bool WebPage::javaScriptPrompt(QWebFrame *originatingFrame, const QString& msg, const QString& defaultValue, QString* result)
{
	return true;
}
