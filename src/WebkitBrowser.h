#ifndef WEBKITBROWSER_H_
#define WEBKITBROWSER_H_

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QImage>
#include <QPainter>
#include <QPrinter>
#include <qwaitcondition.h>

#include "WebPage.h"
#include "FileSystem.h"

class Error
{
	public:
		 Error(){}
		~Error(){}

		QString	message;
};

class WebkitBrowser : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant RESPONSE READ RESPONSE)
    Q_PROPERTY(QVariant CONTENT READ CONTENT)

	public:
		 WebkitBrowser(QObject *parent = 0);
		~WebkitBrowser();

	private:
		bool        			_webPageLoaded;
		int         			_timeElapsed;
		int         			_timeout;
		QTimer	     			_timer;
		QString					_userAgent;
		QNetworkAccessManager	_manager;
		WebPage					_webPage;
		QMap<QString, QVariant>	_requestOptions;
		QMap<QString, QVariant> _response;
		QMap<QString, QVariant> _proxy;
		QByteArray				_content;
		QNetworkCookieJar*		_cookieJar;

		QVariant	getRequestOption(QString key, QVariant defaultValue);

	public slots:
		QVariant	RESPONSE();
		QVariant	CONTENT();
		void    	request(QVariant options);
		void     	wait(int seconds, QString assertJavascript = "");
		QVariant 	evaluateJS(QString javascript);

		QString  	getHTML();
		QString  	getText();
		void	 	saveBMP(QString fileName);
		void     	savePDF(QString fileName);

		void		setProxy(QVariant proxyM);
		QVariant	getProxy();
		void		setUserAgent(QString userAgent);
		QString		getUserAgent();

	private slots:
		void slotUpdateTimeElapsed();
		void slotManagerFinished(QNetworkReply* reply);
		void slotLoadFinished(bool ok);
		void slotLoadStarted();
};

#endif
