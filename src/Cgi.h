#ifndef CGI_H_
#define CGI_H_

#include <QtCore>

class Cgi
{
	public:
		QMap<QString, QVariant> ENVIROMENT;
		QMap<QString, QVariant> GET;
		QMap<QString, QVariant> POST;

		 Cgi();
		~Cgi();

		QString	getEnviromentValue(QString name);
		void	output(QString output, int code = 200, QString codeMessage = "OK", QString contentType = "text/html");

	private:
		void setEnviroment();
		void setGetVariables();
		void setPostVariables();
		void getQueryParams(QString queryString, QMap<QString, QVariant>* map);

	};

#endif /*CGI_H_*/
